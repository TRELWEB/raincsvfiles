# RAIN CSV FILES

The service is to ingest csv and process

## How to run locally

1. Open your docker and run docker-compose.yaml
2. Use the script that comes in `resources/database/migrations` and create your table and database
3. Go to `resources/config.yaml` file
4. Update the file with your port
5. Now it's running
6. Open the folder resources and you can find the postman collection
7. import into your own postman and now you can test

## Documentation

[Check API docs](/docs/openapi.yml)

## Architecture Project

We decide to use the architecture of microservices.

### Why?

Because we now that right now all the software, or at least kind of all
is focused on client, then we can do more scalable software, and we can
create software with too much options of implementations.

### How you would evolve your submitted code
Ok the first part, it's about the file types that we can receive.
1. we can receive more than just csv.
2. part could be to storage the files not locally, we can put it in a cloud storage. 
3. could be creating a job to delete all the correct data before storage the information.
4. maybe the structs that we receive we can receive more than just employee.