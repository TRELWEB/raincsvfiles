package main

import (
	"github.com/go-playground/validator/v10"
	"gitlab.com/rain/csv/processor/config"
	"gitlab.com/rain/csv/processor/internal/adapters/api"
	"gitlab.com/rain/csv/processor/internal/adapters/db"
	"gitlab.com/rain/csv/processor/internal/repositories/employee"
	"gitlab.com/rain/csv/processor/internal/services/csv"
	employeesvc "gitlab.com/rain/csv/processor/internal/services/employee"
)

func main() {
	// Logger
	logger := config.NewLogger()
	defer config.CloseLogger(logger)

	// Configs
	configs := config.LoadConfig(logger)

	// Http Router
	httpServer := api.NewHTTPServer(logger, configs.Server)

	// Validator
	validate := validator.New()

	// Database Connection
	database := db.NewDatabaseConnection(logger, configs.Database)

	// Dependencies
	employeeRepo := employee.NewDatabaseRepository(logger, database)

	//Services
	csvService := csv.NewDefaultService(logger, validate, employeeRepo)
	employeeService := employeesvc.NewDefaultService(logger, employeeRepo)

	// Controllers
	api.NewHealthController(httpServer)
	api.NewCSVController(httpServer, validate, csvService)
	api.NewEmployeeController(httpServer, validate, employeeService)

	// Start
	httpServer.Start()

}
