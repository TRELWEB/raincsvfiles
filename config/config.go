package config

import (
	"github.com/knadh/koanf"
	"github.com/knadh/koanf/parsers/yaml"
	"github.com/knadh/koanf/providers/file"
	"go.uber.org/zap"
)

type Configurations struct {
	Server   ServerConfigurations   `koanf:"server"`
	Database DatabaseConfigurations `koanf:"database"`
	Keys     KeysConfigurations     `koanf:"keys"`
}

// ServerConfigurations Server configurations
type ServerConfigurations struct {
	Port int `koanf:"port"`
}

type DatabaseConfigurations struct {
	Dsn  string `koanf:"dsn"`
	Pool int    `koanf:"pool"`
}

type KeysConfigurations struct {
	Public  string `koanf:"public"`
	Private string `koanf:"private"`
}

// LoadConfig Loads configurations depending upon the environment
func LoadConfig(logger *zap.SugaredLogger) *Configurations {
	k := koanf.New(".")
	err := k.Load(file.Provider("resources/config.yml"), yaml.Parser())
	if err != nil {
		logger.Fatalf("Failed to locate configurations. %v", err)
	}

	var configuration Configurations

	err = k.Unmarshal("", &configuration)
	if err != nil {
		logger.Fatalf("Failed to load configurations. %v", err)
	}

	return &configuration
}
