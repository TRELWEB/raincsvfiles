package config

import (
	"go.elastic.co/ecszap"
	"go.uber.org/zap"
	"os"
)

func NewLogger() *zap.SugaredLogger {
	encoderConfig := ecszap.NewDefaultEncoderConfig()
	core := ecszap.NewCore(encoderConfig, os.Stdout, zap.DebugLevel)
	logger := zap.New(core, zap.AddCaller())
	return logger.Sugar()
}

func CloseLogger(logger *zap.SugaredLogger) {
	_ = logger.Sync()
}
