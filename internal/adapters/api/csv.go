package api

import (
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/go-playground/validator/v10"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	rerrors "gitlab.com/rain/csv/processor/internal/adapters/rErrors"
	scsv "gitlab.com/rain/csv/processor/internal/services/csv"
	"go.uber.org/zap"
	"mime"
	"mime/multipart"
	"net/http"
)

// CSVController Admin controller
type CSVController struct {
	log           *zap.SugaredLogger
	validate      *validator.Validate
	counterMetric prometheus.Counter
	csvs          scsv.ICSVService
}

// NewCSVController Constructor
func NewCSVController(server *HTTPServer, validator *validator.Validate, csvs scsv.ICSVService) *CSVController {
	csvc := &CSVController{
		log:      server.Logger,
		validate: validator,
		csvs:     csvs,
		counterMetric: promauto.NewCounter(prometheus.CounterOpts{
			Name: "csv_reqs_total",
			Help: "The total number of requests to admin endpoints",
		}),
	}

	// Load routes
	server.Router.Group(func(r chi.Router) {
		server.Router.Post("/v1/csv/ingest", csvc.HandleGetCSVFile)
	})

	return csvc
}

func (csvc *CSVController) HandleGetCSVFile(w http.ResponseWriter, r *http.Request) {
	traceID := r.Context().Value(middleware.RequestIDKey).(string)
	err := r.ParseMultipartForm(32 << 20) // maxMemory 32MB
	if err != nil {
		RenderError(r.Context(), w, rerrors.BadRequest(rerrors.ErrBadRequest, "File exceeded maximum memory", map[string]string{"": ""}))
		return
	}

	var file multipart.File
	var params map[string]string
	mFile := r.MultipartForm.File
	if len(mFile[KeyFileName]) == 1 {
		multipartFile := mFile[KeyFileName][0]
		_, params, err = mime.ParseMediaType(multipartFile.Header.Get("Content-Disposition"))
		if multipartFile.Header.Get("Content-Type") == "text/csv" {
			file, err = multipartFile.Open()
			if err != nil {
				csvc.log.With("traceID", traceID).
					With("FileName: ", params["filename"]).
					With("KeyName: ", params["name"]).
					Errorf("Error while open file %e", err)
			}
		}
	} else {
		csvc.log.With("traceID", traceID).
			With("Number Files: ", len(mFile[KeyFileName])).
			Errorf("Key was incorrect or we receive more than 1 file")
		RenderError(r.Context(), w, rerrors.BadRequest(rerrors.ErrBadRequest, rerrors.ErrBadRequest, map[string]string{"": ""}))
		return
	}

	res, err := csvc.csvs.IngestCSVFile(r.Context(), file)
	if err != nil {
		RenderError(r.Context(), w, err)
		return
	}

	RenderJSON(r.Context(), w, http.StatusOK, res)
}
