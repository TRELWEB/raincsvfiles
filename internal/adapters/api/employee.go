package api

import (
	"github.com/go-chi/chi/v5"
	"github.com/go-playground/validator/v10"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"gitlab.com/rain/csv/processor/internal/adapters/rErrors"
	"gitlab.com/rain/csv/processor/internal/domains/pagination"
	"gitlab.com/rain/csv/processor/internal/services/employee"
	"go.uber.org/zap"
	"net/http"
	"strconv"
)

// EmployeeController Admin controller
type EmployeeController struct {
	log           *zap.SugaredLogger
	validate      *validator.Validate
	counterMetric prometheus.Counter
	employeeSvc   employee.IEmployeeService
}

// NewEmployeeController Constructor
func NewEmployeeController(server *HTTPServer, validator *validator.Validate, esvc employee.IEmployeeService) *EmployeeController {
	ec := &EmployeeController{
		log:         server.Logger,
		validate:    validator,
		employeeSvc: esvc,
		counterMetric: promauto.NewCounter(prometheus.CounterOpts{
			Name: "employee_reqs_total",
			Help: "The total number of requests to admin endpoints",
		}),
	}

	// Load routes
	server.Router.Group(func(r chi.Router) {
		r.Post("/v1/employee/", ec.handleNewEmployee)
		r.Get("/v1/employee/", ec.handleGetAllEmployee)
		r.Get("/v1/employee/{id}", ec.handleGetEmployeeDetails)
	})

	return ec
}

func (ec *EmployeeController) handleNewEmployee(w http.ResponseWriter, r *http.Request) { /*tr, err := csv.transactionSvc.GetAllTransactions(page, size)
	if err != nil {
		csv.log.Errorf("Error while finding transactions: %s", err)
		terr := rerrors.New(rerrors.ErrInternalService, "Internal error", map[string]string{})
		renderTerror(r.Context(), w, terr)
		return
	}*/

	RenderJSON(r.Context(), w, http.StatusOK, nil)
}

func (ec *EmployeeController) handleGetAllEmployee(w http.ResponseWriter, r *http.Request) {
	query := r.URL.Query()
	from, _ := strconv.Atoi(query.Get("page"))
	size, _ := strconv.Atoi(query.Get("size"))
	filter := pagination.Filter{
		From: from,
		Size: size,
	}

	result, err := ec.employeeSvc.FindAll(r.Context(), &filter)
	if err != nil {
		ec.log.Errorf("Error while finding employee: %s", err)
		terr := rerrors.New(rerrors.ErrInternalService, "Internal error", map[string]string{})
		RenderRerror(r.Context(), w, terr)
		return
	}

	RenderJSON(r.Context(), w, http.StatusOK, result)
}

func (ec *EmployeeController) handleGetEmployeeDetails(w http.ResponseWriter, r *http.Request) {
	var ID = chi.URLParam(r, "id")

	err := ec.validate.Var(ID, "required")
	if err != nil {
		ec.log.Error("Employee Id is required")
		terr := rerrors.New(rerrors.ErrPreconditionFailed, "Employee Id is required", map[string]string{})
		RenderRerror(r.Context(), w, terr)
		return
	}

	em, err := ec.employeeSvc.FindById(r.Context(), ID)
	if err != nil {
		ec.log.Errorf("Error while finding employee: %s", err)
		terr := rerrors.New(rerrors.ErrInternalService, "Internal error", map[string]string{})
		RenderRerror(r.Context(), w, terr)
		return
	}

	RenderJSON(r.Context(), w, http.StatusOK, em)
}
