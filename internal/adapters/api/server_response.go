package api

import (
	"context"
	"encoding/json"
	"gitlab.com/rain/csv/processor/internal/adapters/rErrors"
	"net/http"

	"github.com/go-chi/chi/v5/middleware"
)

// RenderJSON Render A helper function to render a JSON response
func RenderJSON(ctx context.Context, w http.ResponseWriter, httpStatusCode int, payload interface{}) {
	// Headers
	w.Header().Set(middleware.RequestIDHeader, middleware.GetReqID(ctx))
	w.Header().Set("Content-Type", "application/json")

	js, err := json.Marshal(payload)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(httpStatusCode)
	_, _ = w.Write(js)
}

// RenderRerror Renders a terror with some sane defaults
func RenderRerror(ctx context.Context, w http.ResponseWriter, rerror *rerrors.Error) {
	var httpStatusCode int

	if rerror.PrefixMatches(rerrors.ErrPreconditionFailed) || rerror.PrefixMatches(rerrors.ErrBadRequest) {
		httpStatusCode = http.StatusBadRequest
	} else if rerror.PrefixMatches(rerrors.ErrUnauthorized) {
		httpStatusCode = http.StatusUnauthorized
	} else if rerror.PrefixMatches(rerrors.ErrNotFound) {
		httpStatusCode = http.StatusNotFound
	} else {
		httpStatusCode = http.StatusInternalServerError
	}

	payload := map[string]string{
		"code":    rerror.Code,
		"message": rerror.Message,
	}

	RenderJSON(ctx, w, httpStatusCode, payload)
}

// RenderError Renders an error with some sane defaults.
// This function receive any type of error, but is recommended use a terror
// for cases when you what to send a specific status code, because other kind
// of errors are handled as internal_errors
func RenderError(ctx context.Context, w http.ResponseWriter, err error) {
	var httpStatusCode int
	var code string
	var message string

	// Check if error can be parsed as terror
	if terr, ok := err.(*rerrors.Error); ok {
		if terr.PrefixMatches(rerrors.ErrPreconditionFailed) {
			httpStatusCode = http.StatusPreconditionFailed
		} else if terr.PrefixMatches(rerrors.ErrConflict) {
			httpStatusCode = http.StatusConflict
		} else if terr.PrefixMatches(rerrors.ErrBadRequest) {
			httpStatusCode = http.StatusBadRequest
		} else if terr.PrefixMatches(rerrors.ErrUnauthorized) {
			httpStatusCode = http.StatusUnauthorized
		} else if terr.PrefixMatches(rerrors.ErrForbidden) {
			httpStatusCode = http.StatusForbidden
		} else if terr.PrefixMatches(rerrors.ErrNotFound) {
			httpStatusCode = http.StatusNotFound
		} else {
			httpStatusCode = http.StatusInternalServerError
		}
		// Assign Code and Error message
		code = terr.Code
		message = terr.Message
	} else {
		// All errors that not implement terror will be parsed as a general internal error
		// with a default error message.
		httpStatusCode = http.StatusInternalServerError
		code = rerrors.ErrInternalService
		message = "something went wrong...."
	}

	payload := map[string]string{
		"code":    code,
		"message": message,
	}

	RenderJSON(ctx, w, httpStatusCode, payload)
}
