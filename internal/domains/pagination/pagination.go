package pagination

// Filter is the base filter model
type Filter struct {
	From int
	Size int
}

// SanitizePageFilter Handles the sanitization for the values in query parameters
func (f *Filter) SanitizePageFilter() error {
	// Set default offset
	if f.From < MinimumFromValue {
		f.From = MinimumFromValue
	}
	// Set default size
	if f.Size < MinimumSizeValue {
		f.Size = DefaultSizeValue
	} else if f.Size > MaximumSizeValue {
		f.Size = MaximumSizeValue
	}

	return nil
}

// PaginatedRes generic model to paginate all API responses
type PaginatedRes struct {
	Data interface{} `json:"var"`
	From int         `json:"from"`
	Size int         `json:"size"`
}
