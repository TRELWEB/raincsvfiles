package employee

import (
	"context"
	"gitlab.com/rain/csv/processor/internal/domains/pagination"
	"time"

	"github.com/shopspring/decimal"
	"github.com/uptrace/bun"
	"go.uber.org/zap"
)

// DatabaseRepository Cockroachdb repository implementation for payments
type DatabaseRepository struct {
	log *zap.SugaredLogger
	db  *bun.DB
}

// NewDatabaseRepository Creates a new repository
func NewDatabaseRepository(logger *zap.SugaredLogger, conn *bun.DB) *DatabaseRepository {
	return &DatabaseRepository{
		log: logger,
		db:  conn,
	}
}

// Create new employee
func (d *DatabaseRepository) Create(ctx context.Context, model *Model) error {
	_, err := d.db.NewInsert().Model(model).Exec(ctx)
	if err != nil {
		return err
	}

	return nil
}

// Update update a single employee
func (d *DatabaseRepository) Update(ctx context.Context, payOutID string, payerAmount int64, fxRate decimal.Decimal, status string) error {
	_, err := d.db.NewUpdate().Table("employee").
		Where("id = ?", payOutID).
		Set("payer_amount = ?", payerAmount).
		Set("fx_rate = ?", fxRate).
		Set("status = ?", status).
		Set("updated_at = ?", time.Now().UTC()).
		Exec(ctx)

	if err != nil {
		return err
	}

	return nil
}

// FindAll get the list of employee's with details
func (d *DatabaseRepository) FindAll(ctx context.Context, filter *pagination.Filter) ([]Model, error) {
	employees := []Model{}
	query := d.db.NewSelect().Model(&employees).
		Order("id asc").
		Limit(filter.Size).
		Offset(filter.Size * (filter.From - 1))

	if err := query.Scan(ctx); err != nil {
		return employees, err
	}

	return employees, nil
}

// FindByID Find employee details by id
func (d *DatabaseRepository) FindByID(ctx context.Context, ID string) (*Model, bool) {
	employee := Model{}

	if err := d.db.NewSelect().Model(&employee).Where("id = ?", ID).Scan(ctx); err != nil {
		d.log.Errorf("Cannot retrieve pay-in: %s", err.Error())
		return nil, false
	}

	return &employee, true
}
