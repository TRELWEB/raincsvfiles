package employee

import "github.com/uptrace/bun"

// Model Database model for employees
type Model struct {
	bun.BaseModel `bun:"table:employee"`

	ID     string `bun:"id,pk"`
	Name   string `bun:"name"`
	Salary int64  `bun:"salary"`
	Email  string `bun:"email"`
}
