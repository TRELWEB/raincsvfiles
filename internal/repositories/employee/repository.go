package employee

import (
	"context"
	"github.com/shopspring/decimal"
	"gitlab.com/rain/csv/processor/internal/domains/pagination"
)

// IRepository Payments repository interface
type IRepository interface {
	Create(ctx context.Context, model *Model) error
	Update(ctx context.Context, payOutID string, payerAmount int64, fxRate decimal.Decimal, status string) error
	FindAll(ctx context.Context, filter *pagination.Filter) ([]Model, error)
	FindByID(ctx context.Context, ID string) (*Model, bool)
}
