package csv

// Headers of csv file
const (
	FirstName = "FIRSTNAME"
	LastName  = "LASTNAME"
	Wage      = "WAGE"
	Email     = "EMAIL"
	ID        = "ID"
)
