package csv

import (
	"context"
	"encoding/csv"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/go-playground/validator/v10"
	"gitlab.com/rain/csv/processor/internal/domains/pagination"
	"gitlab.com/rain/csv/processor/internal/repositories/employee"
	"gitlab.com/rain/csv/processor/internal/util"
	"go.uber.org/zap"
	"math"
	"mime/multipart"
	"net/mail"
	"strconv"
	"strings"
)

// DefaultService Default implementation
type DefaultService struct {
	log          *zap.SugaredLogger
	validate     *validator.Validate
	employeeRepo employee.IRepository
}

// NewDefaultService Creates a new instance of the CSV processor service
func NewDefaultService(logger *zap.SugaredLogger, v *validator.Validate, er employee.IRepository) *DefaultService {
	return &DefaultService{
		log:          logger,
		validate:     v,
		employeeRepo: er,
	}
}

// IngestCSVFile Ingest csv files and store employee
func (dcsvc *DefaultService) IngestCSVFile(ctx context.Context, file multipart.File) (*[]EmployeeResponse, error) {
	traceID := ctx.Value(middleware.RequestIDKey).(string)
	dcsvc.log.With("traceId", traceID).Infof("Ingesting CSV")

	// Read, Process & Validate csv
	employeePldList := dcsvc.readAndProcessCSV(ctx, file)
	// Validate, Storage & Create csv
	employeeProcessedList := dcsvc.validateCreateAndStorageEmployeeList(ctx, employeePldList)

	return toEmployeeResponse(employeeProcessedList)
}

func (dcsvc *DefaultService) readAndProcessCSV(ctx context.Context, file multipart.File) []EmployeePld {
	traceID := ctx.Value(middleware.RequestIDKey).(string)
	csvReader := csv.NewReader(file)
	if csvReader.InputOffset() <= int64(0) {
		dcsvc.log.With("traceId", traceID).Errorf("Error on read file, file is empty")
		return nil
	}
	data, err := csvReader.ReadAll()
	if err != nil {
		dcsvc.log.With("traceId", traceID).Errorf("Error on read file %e", err)
		return nil
	}

	dcsvc.log.With("traceId", traceID).Infof("Reading CSV file & creating employee payload list")
	// convert csv lines to array of structs
	var employeeList []EmployeePld
	headerMap := make(map[string]int)
	for i, line := range data {
		if i == 0 {
			dcsvc.log.With("traceId", traceID).Infof("Validate Headers")
			for i, v := range line {
				v = strings.ToUpper(v)
				headerMap[validateHeaders(v)] = i
			}
		} else if i > 0 {
			dcsvc.log.With("traceId", traceID).
				With("RowNumber: ", i).
				Infof("Validate Fields")
			var singleEmployee EmployeePld
			employeeList = append(employeeList, validateFields(line, headerMap, singleEmployee))
		}
	}
	return employeeList
}

func (dcsvc *DefaultService) validateCreateAndStorageEmployeeList(ctx context.Context, employeeList []EmployeePld) []EmployeePld {
	traceID := ctx.Value(middleware.RequestIDKey).(string)
	dcsvc.log.With("traceId", traceID).Infof("Validating CSV file")
	var processedEmployeeList []EmployeePld
	employeeModels, _ := dcsvc.employeeRepo.FindAll(ctx, &pagination.Filter{
		From: 1,
		Size: 100,
	})
	for k := range employeeList {
		singleEmployee := employeeList[k]
		err := dcsvc.validate.Struct(singleEmployee)
		if err != nil {
			singleEmployee.Valid = false
		} else {
			singleEmployee.Valid = true
		}

		for y := range employeeModels {
			singleEmployeeModel := employeeModels[y]
			if singleEmployeeModel.ID == singleEmployee.ID {
				singleEmployee.Valid = false
			}
		}

		processedEmployeeList = append(processedEmployeeList, singleEmployee)
	}
	//Storage CSV
	dcsvc.storeEmployees(ctx, processedEmployeeList)
	//Create CSV file
	dcsvc.createCSVFile(ctx, processedEmployeeList)
	return processedEmployeeList
}

func (dcsvc *DefaultService) storeEmployees(ctx context.Context, employeeList []EmployeePld) {
	traceID := ctx.Value(middleware.RequestIDKey).(string)
	dcsvc.log.With("traceId", traceID).Infof("Storing employees acepted")
	for k := range employeeList {
		singleEmployeePld := employeeList[k]
		if singleEmployeePld.Valid {
			singleEmployeeModel := employee.Model{
				ID:     singleEmployeePld.ID,
				Email:  singleEmployeePld.Email,
				Name:   singleEmployeePld.FirstName + " " + singleEmployeePld.LastName,
				Salary: singleEmployeePld.Wage,
			}
			err := dcsvc.employeeRepo.Create(ctx, &singleEmployeeModel)
			if err != nil {
				dcsvc.log.With("traceId", traceID).Errorf("Error on storage employee with ID: %s", singleEmployeePld.ID)
			}
		}
	}
}

func (dcsvc *DefaultService) createCSVFile(ctx context.Context, employees []EmployeePld) {
	traceID := ctx.Value(middleware.RequestIDKey).(string)
	dcsvc.log.With("traceId", traceID).Infof("Creating CSV file")

	var correctData [][]string
	var badData [][]string
	headers := []string{ID, FirstName, LastName, Wage, Email}
	correctData = append(correctData, headers)
	badData = append(badData, headers)
	for _, record := range employees {
		row := []string{record.ID, record.FirstName, record.LastName, strconv.FormatInt(record.Wage, 10), record.Email}
		if record.Valid {
			correctData = append(correctData, row)
		} else {
			badData = append(badData, row)
		}
	}
	util.CsvWriterData(correctData, "correctData")
	util.CsvWriterData(badData, "badData")
}

func validateFields(data []string, headerMap map[string]int, singleEmployee EmployeePld) EmployeePld {
	if wValue, wOk := headerMap[Wage]; wOk {
		if wage, ok := validateWage(data[wValue]); ok {
			singleEmployee.Wage = wage
		}
	}
	if fnValue, fnOk := headerMap[FirstName]; fnOk {
		singleEmployee.FirstName = data[fnValue]
	}
	if lnValue, lnOk := headerMap[LastName]; lnOk {
		singleEmployee.LastName = data[lnValue]
	}
	if eValue, eOk := headerMap[Email]; eOk {
		if email, ok := validateMailAddress(data[eValue]); ok {
			singleEmployee.Email = email
		}
	}
	if iValue, iOk := headerMap[ID]; iOk {
		if id, ok := validateID(data[iValue]); ok {
			singleEmployee.ID = id
		}
	}
	return singleEmployee
}

func validateHeaders(header string) string {
	if strings.Contains(header, "LAST") {
		return LastName
	} else if strings.Contains(header, "L.") {
		return LastName
	} else if strings.Contains(header, "FIRST") {
		return FirstName
	} else if strings.Contains(header, "NAME") {
		return FirstName
	} else if strings.Contains(header, "F.") {
		return FirstName
	} else if strings.Contains(header, "SALARY") {
		return Wage
	} else if strings.Contains(header, "WAGE") {
		return Wage
	} else if strings.Contains(header, "RATE") {
		return Wage
	} else if strings.Contains(header, "MAIL") {
		return Email
	} else if strings.Contains(header, "ID") {
		return ID
	} else if strings.Contains(header, "EMPLOYEE NUMBER") {
		return ID
	} else {
		return header
	}
}

func validateMailAddress(address string) (string, bool) {
	addr, err := mail.ParseAddress(address)
	if err != nil {
		return "", false
	}
	return addr.Address, true
}

func validateWage(Wage string) (int64, bool) {
	var fullSalary int64
	if strings.Contains(Wage, "$") {
		Wage = strings.Trim(Wage, "$")
	} else if strings.Contains(Wage, ",") {
		Wage = strings.Replace(Wage, ",", "", 1)
	}
	s, err := strconv.ParseFloat(Wage, 128)
	if err != nil {
		return 0, false
	}
	fullSalary = int64(math.Ceil(s * 100))
	return fullSalary, true
}

func validateID(ID string) (string, bool) {
	if strings.Contains(ID, "RT") {
		return ID, true
	} else {
		return "", false
	}
}
