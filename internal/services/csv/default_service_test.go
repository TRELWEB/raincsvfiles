package csv

import (
	"context"
	"fmt"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/rain/csv/processor/internal/repositories/employee"
	"gitlab.com/rain/csv/processor/internal/repositories/employee/employeemock"
	"mime/multipart"
	"os"
	"testing"
)

func TestDefaultCSVService_IngestCSVFile(t *testing.T) {
	type fields struct {
		employeeRepo     *employeemock.IRepository
		employeeRepoFunc func() *employeemock.IRepository
	}
	type args struct {
		ctx     context.Context
		payload multipart.File
		id      string
	}
	type assertsParams struct {
		fields
		args
		result *[]EmployeeResponse
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
		asserts func(*testing.T, error, assertsParams) bool
	}{
		{
			name: "File fail",
			fields: fields{
				employeeRepoFunc: func() *employeemock.IRepository {
					employeeRepoMock := new(employeemock.IRepository)
					employeeRepoMock.On("FindAll", mock.Anything, mock.Anything).
						Return([]employee.Model{{ID: "RT3", Name: "Test find by id"}}, nil)
					employeeRepoMock.On("Create", mock.Anything, mock.Anything).
						Return(nil)
					return employeeRepoMock
				},
			},
			args: args{
				payload: nil,
			},
			wantErr: true,
			asserts: func(t *testing.T, err error, ap assertsParams) bool {
				return assert.Errorf(t, err, "Error anyone was accepted")
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if tt.args.ctx == nil {
				tt.args.ctx = ctx
			}
			if tt.fields.employeeRepoFunc != nil {
				tt.fields.employeeRepo = tt.fields.employeeRepoFunc()
			}
			s := NewDefaultService(log, validate, tt.fields.employeeRepo)
			response, err := s.IngestCSVFile(tt.args.ctx, tt.args.payload)
			if (err != nil) != tt.wantErr {
				t.Errorf("DefaultService.IngestCSVFile() error = %v, wantErr %v", err, tt.wantErr)
			}
			assertsParams := assertsParams{
				fields: tt.fields,
				args:   tt.args,
				result: response,
			}
			if !tt.asserts(t, err, assertsParams) {
				t.Errorf("Assert error on test = '%v'", tt.name)
			}
		})
	}

}

func openFile() multipart.File {
	file, err := os.OpenFile("../../../resources/test_file/roster2.csv", os.O_RDONLY, 0644)
	if err != nil {
		fmt.Printf("Failed to read file: %s", "fileName")
	}
	defer file.Close()
	return file
}

//mockery --name=IRepository --filename employee_repository_mock.go  --output employeemock --outpkg employeemock
