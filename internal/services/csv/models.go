package csv

import (
	rerrors "gitlab.com/rain/csv/processor/internal/adapters/rErrors"
	"strconv"
)

// EmployeePld var of employee
type EmployeePld struct {
	ID        string `json:"id" validate:"required"`
	FirstName string `json:"firstName" validate:"required"`
	LastName  string `json:"lastName"`
	Wage      int64  `json:"wage" validate:"required"`
	Email     string `json:"email" validate:"required"`
	Valid     bool
}

// EmployeeResponse var of employee
type EmployeeResponse struct {
	ID    string `json:"id" validate:"required"`
	Name  string `json:"name" validate:"required"`
	Wage  string `json:"wage" validate:"required"`
	Email string `json:"email" validate:"required"`
}

// EmployeePldToEmployeeResponse employee mapper
func toEmployeeResponse(employeeList []EmployeePld) (*[]EmployeeResponse, error) {
	var response []EmployeeResponse
	for k := range employeeList {
		if employeeList[k].Valid {
			response = append(response, EmployeeResponse{
				ID:    employeeList[k].ID,
				Name:  employeeList[k].FirstName + " " + employeeList[k].LastName,
				Wage:  "$" + strconv.FormatInt(employeeList[k].Wage/100, 10),
				Email: employeeList[k].Email,
			})
		}
	}
	if len(response) <= 0 {
		return nil, rerrors.New(rerrors.ErrNotFound, "Error anyone was accepted", map[string]string{})
	}
	return &response, nil
}
