package csv

import (
	"context"
	"mime/multipart"
)

// ICSVService Operations over beneficiaries
type ICSVService interface {
	IngestCSVFile(ctx context.Context, file multipart.File) (*[]EmployeeResponse, error)
}
