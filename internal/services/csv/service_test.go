package csv

import (
	"context"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/go-playground/validator/v10"
	"go.elastic.co/ecszap"
	"go.uber.org/zap"
	"os"
	"testing"
)

var ctx context.Context
var log *zap.SugaredLogger
var validate *validator.Validate

func TestMain(m *testing.M) {
	log = NewLogger()
	ctx = context.Background()
	ctx = context.WithValue(ctx, middleware.RequestIDKey, "unit-test-csv-id")
	// Run test cases
	exitVal := m.Run()
	os.Exit(exitVal)
}

func NewLogger() *zap.SugaredLogger {

	encoderConfig := ecszap.NewDefaultEncoderConfig()
	core := ecszap.NewCore(encoderConfig, os.Stdout, zap.DebugLevel)
	logger := zap.New(core, zap.AddCaller())
	return logger.Sugar()
}
