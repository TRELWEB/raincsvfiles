package employee

import (
	"context"
	"github.com/go-chi/chi/v5/middleware"
	lop "github.com/samber/lo/parallel"
	rerrors "gitlab.com/rain/csv/processor/internal/adapters/rErrors"
	"gitlab.com/rain/csv/processor/internal/domains/pagination"
	"gitlab.com/rain/csv/processor/internal/repositories/employee"
	"go.uber.org/zap"
)

// DefaultService Default implementation
type DefaultService struct {
	log          *zap.SugaredLogger
	employeeRepo employee.IRepository
}

// NewDefaultService Creates a new instance of the beneficiary service
func NewDefaultService(logger *zap.SugaredLogger, er employee.IRepository) *DefaultService {
	return &DefaultService{
		log:          logger,
		employeeRepo: er,
	}
}

// Create function to create employee
func (des *DefaultService) Create(ctx context.Context, payload *Pld) error {
	err := des.employeeRepo.Create(ctx, &employee.Model{
		ID:     payload.ID,
		Name:   payload.FirstName + " " + payload.LastName,
		Salary: payload.Wage,
		Email:  payload.Email,
	})
	if err != nil {
		return err
	}
	return nil
}

// FindById Find employee by ID
func (des *DefaultService) FindById(ctx context.Context, ID string) (*employee.Model, error) {
	traceID := ctx.Value(middleware.RequestIDKey).(string)
	des.log.With("traceId", traceID).Infof("Retrieving employee [%s]", ID)

	employeeRes, ok := des.employeeRepo.FindByID(ctx, ID)
	if !ok {
		return nil, rerrors.New(rerrors.ErrNotFound, "Employee not found", map[string]string{})
	}
	return employeeRes, nil
}

// FindAll Find all employee's
func (des *DefaultService) FindAll(ctx context.Context, filter *pagination.Filter) (*pagination.PaginatedRes, error) {
	traceID := ctx.Value(middleware.RequestIDKey)
	des.log.With("traceID", traceID).Infof("Retrieving employees")

	err := filter.SanitizePageFilter()
	if err != nil {
		des.log.With("traceId", traceID).
			Errorf("Error while sanitizing parameters [%#v] error [%v]", filter, err)
		return nil, err
	}

	res, err := des.employeeRepo.FindAll(ctx, filter)
	if err != nil {
		des.log.With("traceId", traceID).
			Errorf("Error while retrieving payment orders: %v", err)
		return nil, err
	}

	// Mapping repository response
	items := lop.Map(res, func(p employee.Model, _ int) MinifiedModel {
		return toMinifiedModel(p)
	})

	return &pagination.PaginatedRes{
		Data: items,
		From: filter.From,
		Size: filter.Size,
	}, nil
}
