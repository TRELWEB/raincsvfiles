package employee

import (
	"context"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/rain/csv/processor/internal/domains/pagination"
	"gitlab.com/rain/csv/processor/internal/repositories/employee"
	"gitlab.com/rain/csv/processor/internal/repositories/employee/employeemock"
	"testing"
)

func TestDefaultEmployeeService_Create(t *testing.T) {
	type fields struct {
		employeeRepo     *employeemock.IRepository
		employeeRepoFunc func() *employeemock.IRepository
	}
	type args struct {
		ctx     context.Context
		payload *Pld
		id      string
	}
	type assertsParams struct {
		fields
		args
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
		asserts func(*testing.T, error, assertsParams) bool
	}{
		{
			name: "Happy Path Create",
			fields: fields{
				employeeRepoFunc: func() *employeemock.IRepository {
					employeeRepoMock := new(employeemock.IRepository)
					employeeRepoMock.On("Create", mock.Anything, mock.Anything).
						Return(nil)
					return employeeRepoMock
				},
			},
			args: args{
				payload: &Pld{},
			},
			wantErr: false,
			asserts: func(t *testing.T, err error, ap assertsParams) bool {
				return assert.NoError(t, err) &&
					ap.employeeRepo.AssertExpectations(t) &&
					ap.fields.employeeRepo.AssertCalled(t, "Create", ap.args.ctx, mock.Anything)
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if tt.args.ctx == nil {
				tt.args.ctx = ctx
			}
			if tt.fields.employeeRepoFunc != nil {
				tt.fields.employeeRepo = tt.fields.employeeRepoFunc()
			}
			s := NewDefaultService(log, tt.fields.employeeRepo)
			err := s.Create(tt.args.ctx, tt.args.payload)
			if (err != nil) != tt.wantErr {
				t.Errorf("DefaultService.Create() error = %v, wantErr %v", err, tt.wantErr)
			}

			assertsParams := assertsParams{
				fields: tt.fields,
				args:   tt.args,
			}
			if !tt.asserts(t, err, assertsParams) {
				t.Errorf("Assert error on test = '%v'", tt.name)
			}
		})
	}

}

func TestDefaultService_FindById(t *testing.T) {
	type fields struct {
		employeeRepo     *employeemock.IRepository
		employeeRepoFunc func() *employeemock.IRepository
	}
	type args struct {
		ctx      context.Context
		employee *employee.Model
		id       string
	}
	type assertsParams struct {
		fields
		args
		result *employee.Model
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
		asserts func(*testing.T, error, assertsParams) bool
	}{
		{
			name: "Happy Path FindByID",
			fields: fields{
				employeeRepoFunc: func() *employeemock.IRepository {
					employeeRepoMock := new(employeemock.IRepository)
					employeeRepoMock.On("FindByID", mock.Anything, mock.Anything).
						Return(
							&employee.Model{ID: "RT3", Name: "Test find by id"},
							true)
					return employeeRepoMock
				},
			},
			args: args{
				id: "RT3",
			},
			wantErr: false,
			asserts: func(t *testing.T, err error, ap assertsParams) bool {
				return ap.employeeRepo.AssertExpectations(t) &&
					ap.fields.employeeRepo.AssertCalled(t, "FindByID", ap.args.ctx, "RT3")
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if tt.args.ctx == nil {
				tt.args.ctx = ctx
			}
			if tt.fields.employeeRepoFunc != nil {
				tt.fields.employeeRepo = tt.fields.employeeRepoFunc()
			}
			s := NewDefaultService(log, tt.fields.employeeRepo)
			got, err := s.FindById(tt.args.ctx, tt.args.id)
			if (err != nil) != tt.wantErr {
				t.Errorf("DefaultService.FindById() error = %v, wantErr %v", err, tt.wantErr)
			}

			assertsParams := assertsParams{
				fields: tt.fields,
				args:   tt.args,
				result: got,
			}
			if !tt.asserts(t, err, assertsParams) {
				t.Errorf("Assert error on test = '%v'", tt.name)
			}
		})
	}
}

func TestDefaultService_FindAll(t *testing.T) {
	type fields struct {
		employeeRepo     *employeemock.IRepository
		employeeRepoFunc func() *employeemock.IRepository
	}
	type args struct {
		ctx    context.Context
		filter *pagination.Filter
	}
	type assertsParams struct {
		fields
		args
		result *pagination.PaginatedRes
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
		asserts func(*testing.T, error, assertsParams) bool
	}{
		{
			name: "Happy Path",
			fields: fields{
				employeeRepoFunc: func() *employeemock.IRepository {
					employeeRepoMock := new(employeemock.IRepository)
					employeeRepoMock.On("FindAll", mock.Anything, mock.Anything).
						Return([]employee.Model{{ID: "RT3", Name: "Test find by id"}}, nil)
					return employeeRepoMock
				},
			},
			args: args{
				filter: &pagination.Filter{},
			},
			wantErr: false,
			asserts: func(t *testing.T, err error, ap assertsParams) bool {
				return ap.employeeRepo.AssertExpectations(t) &&
					ap.fields.employeeRepo.AssertCalled(t, "FindAll", ap.args.ctx, mock.Anything)
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if tt.args.ctx == nil {
				tt.args.ctx = ctx
			}
			if tt.fields.employeeRepoFunc != nil {
				tt.fields.employeeRepo = tt.fields.employeeRepoFunc()
			}
			s := NewDefaultService(log, tt.fields.employeeRepo)
			got, err := s.FindAll(tt.args.ctx, tt.args.filter)
			if (err != nil) != tt.wantErr {
				t.Errorf("DefaultService.FindAll() error = %v, wantErr %v", err, tt.wantErr)
			}

			assertsParams := assertsParams{
				fields: tt.fields,
				args:   tt.args,
				result: got,
			}
			if !tt.asserts(t, err, assertsParams) {
				t.Errorf("Assert error on test = '%v'", tt.name)
			}
		})
	}
}
