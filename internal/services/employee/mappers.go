package employee

import "gitlab.com/rain/csv/processor/internal/repositories/employee"

// toMinifiedModel Handles the mapping of a repository model into a service model
func toMinifiedModel(repoEmployee employee.Model) MinifiedModel {
	return MinifiedModel{
		ID:     repoEmployee.ID,
		Name:   repoEmployee.Name,
		Salary: repoEmployee.Salary,
		Email:  repoEmployee.Email,
	}
}
