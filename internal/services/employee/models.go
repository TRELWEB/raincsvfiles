package employee

// MinifiedModel Database model for employee
type MinifiedModel struct {
	ID     string `json:"id"`
	Name   string `json:"customerId"`
	Salary int64  `json:"filename"`
	Email  string `json:"currency"`
}

// Pld EmployeePld var of employee
type Pld struct {
	ID        string `json:"id" validate:"required"`
	FirstName string `json:"firstName" validate:"required"`
	LastName  string `json:"lastName"`
	Wage      int64  `json:"wage" validate:"required"`
	Email     string `json:"email" validate:"required"`
	Valid     bool
}
