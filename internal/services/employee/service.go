package employee

import (
	"context"
	"gitlab.com/rain/csv/processor/internal/domains/pagination"
	"gitlab.com/rain/csv/processor/internal/repositories/employee"
)

// IEmployeeService Operations over beneficiaries
type IEmployeeService interface {
	Create(ctx context.Context, payload *Pld) error
	FindById(ctx context.Context, id string) (*employee.Model, error)
	FindAll(ctx context.Context, filter *pagination.Filter) (*pagination.PaginatedRes, error)
}
