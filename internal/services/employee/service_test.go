package employee

import (
	"context"
	"github.com/go-chi/chi/v5/middleware"
	"go.elastic.co/ecszap"
	"go.uber.org/zap"
	"os"
	"testing"
)

var ctx context.Context
var log *zap.SugaredLogger

func TestMain(m *testing.M) {
	log = NewLogger()
	ctx = context.Background()
	ctx = context.WithValue(ctx, middleware.RequestIDKey, "unit-test-request-id")
	// Run test cases
	exitVal := m.Run()
	os.Exit(exitVal)
}

func NewLogger() *zap.SugaredLogger {
	encoderConfig := ecszap.NewDefaultEncoderConfig()
	core := ecszap.NewCore(encoderConfig, os.Stdout, zap.DebugLevel)
	logger := zap.New(core, zap.AddCaller())
	return logger.Sugar()
}
