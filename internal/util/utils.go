package util

import (
	"encoding/csv"
	"fmt"
	"os"
	"time"
)

func CsvWriterData(data [][]string, fileName string) {
	date := time.Now().Format("2006-01-02.15:04:05")
	file, err := os.Create("upload/" + fileName + "-" + date + ".csv")
	defer func(file *os.File) {
		err := file.Close()
		if err != nil {
			fmt.Println("failed to close file", err)
		}
	}(file)
	if err != nil {
		fmt.Println("failed to open file", err)
	}
	wCorrectData := csv.NewWriter(file)
	defer wCorrectData.Flush()
	err = wCorrectData.WriteAll(data)
	if err != nil {
		return
	}
}
