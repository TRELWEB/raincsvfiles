CREATE DATABASE raincsv

CREATE TABLE public.employee
(
    id     varchar NOT NULL,
    name   varchar NULL,
    salary int NULL,
    email  varchar NULL
);

ALTER TABLE public.employee ADD CONSTRAINT employee_pk PRIMARY KEY (id);
